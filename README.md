# Hong Seng Inventory Tracking #
###[User Stories](https://docs.google.com/document/d/1nefgywDMvYwqjB4xxoy1V3-XomnIXBKaGz1OyE2VC68/edit?usp=sharing) ###

### Setting Up ###

1. Download git [here](https://git-scm.com/download/win) and install.

### Obtain Files ###
1. open command prompt
2. type the following:

```
#!command prompt

$ git clone https://bitbucket.org/Juju-Chang/hongseng.git
```

### Committing and Pushing to the Repository ###
In command prompt:

```
#!command prompt
$ git add .
$ git commit -m "Insert your commit messages here. Description of commit."
$ git push origin master
```
* `.` means to add all files. To add only specific files, replace `.` with specific dir/filenames 

### To Pull Changes ###
In command prompt:

```
#!command prompt
$ git pull origin master

```



#### For more info ####
Contact Juju ~(\*u\*)~